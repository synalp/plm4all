from argparse import ArgumentParser
import os
import torch
from transformers import AutoModelForCausalLM, AutoTokenizer
import time
import idr_torch


parser = ArgumentParser()
parser.add_argument("--models_folder", type=str, help="Huggingface models folder/cache dir",
                    default=os.path.join(os.environ["DSDIR"], "HuggingFace_Models"))  # work on Jean Zay
parser.add_argument("--model_name",  type=str, help="model_name",
                    default="meta-llama/Llama-2-13b-hf")
parser.add_argument("--batch_size", default=1, type=int, help="batch size")
args = parser.parse_args()


def print_rank0(*msg):
    if idr_torch.rank != 0:
        return
    print(*msg)


print_rank0(f"*** Loading the model {args.model_name}")
model_path = os.path.join(args.models_folder, args.model_name)

tokenizer = AutoTokenizer.from_pretrained(model_path)
# Configure the tokenizer to ensure padding is done right
if tokenizer.pad_token is None:
    tokenizer.pad_token_id = 0
tokenizer.padding_side = 'left'

model = AutoModelForCausalLM.from_pretrained(
    model_path,
    torch_dtype=torch.bfloat16,
    device_map="auto"  # Enable Naive Model Parallelism
)

model = model.eval()

# ### Generate ### #

# Inputs of generation here
prompts = ["Hi! How are you ?"]

generate_kwargs = {
    "max_new_tokens": 100,
    "do_sample": True,
}

print_rank0(f"*** Starting to generate {generate_kwargs['max_new_tokens']} tokens with bs={args.batch_size}")
print_rank0(f"Generate args {generate_kwargs}")


def generate():
    """returns a list of zipped inputs, outputs and number of new tokens"""

    input_tokens = tokenizer.batch_encode_plus(prompts, return_tensors="pt", padding=True)
    for t in input_tokens:
        if torch.is_tensor(input_tokens[t]):
            input_tokens[t] = input_tokens[t].to(torch.cuda.current_device())

    outputs = model.generate(**input_tokens, **generate_kwargs)

    input_tokens_lengths = [x.shape[0] for x in input_tokens.input_ids]
    output_tokens_lengths = [x.shape[0] for x in outputs]

    total_new_tokens = [o - i for i, o in zip(input_tokens_lengths, output_tokens_lengths)]
    outputs = tokenizer.batch_decode(outputs, skip_special_tokens=True)

    return zip(prompts, outputs, total_new_tokens)


# warmup is a must if measuring speed as it's when all the optimizations are performed
# e.g. on 8x80 a100 the first pass of 100 tokens takes 23sec, and the next one is 4secs
t_generate_start = time.time()
print_rank0("*** Running generate warmup")
_ = generate()
print_rank0(f"warmup time: {time.time() - t_generate_start}")

print_rank0("*** Running generate")
t_generate_start = time.time()
generated = generate()

print_rank0(f"generate time {time.time() - t_generate_start}")
for i, o, _ in generated:
    print_rank0(f"{'-'*60}\nin={i}\nout={o}\n")
