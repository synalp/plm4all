import os
from time import time

import torch

from transformers import AutoModelForSequenceClassification, AutoTokenizer, BitsAndBytesConfig
from peft import prepare_model_for_kbit_training, LoraConfig, get_peft_model
from datasets import load_from_disk

from tqdm import tqdm
from torchmetrics.classification import BinaryAccuracy

import idr_torch  # IDRIS library to make distribution on JZ easier

# Initialize Distributed Training
DEVICE = torch.device("cuda", idr_torch.local_rank)

# Set Seed for Reproducibility
torch.manual_seed(53)
torch.cuda.manual_seed(53)

# Avoid tokenizers warning
os.environ["TOKENIZERS_PARALLELISM"] = "false"


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    # Check IDRIS documentation to see which datasets and models are available on DSDIR
    parser.add_argument(
        '--data_path',
        type=str,
        default=os.path.join(os.environ["DSDIR"], 'HuggingFace/imdb/plain_text')
    )
    parser.add_argument(
        '--model_dir',
        type=str,
        default=os.path.join(os.environ["DSDIR"], 'HuggingFace_Models/')
    )
    parser.add_argument('--model_name', type=str, default='bigscience/bloom-1b7')
    parser.add_argument('--epochs', type=int, default=1)
    parser.add_argument('--lr', type=float, default=1e-04)
    parser.add_argument('--batch_size', type=int, default=4)
    args = parser.parse_args()
    return args


# print only on rank 0 for cleaner output
def print_rank_0(*args, **kwargs):
    if idr_torch.rank == 0:
        print(*args, **kwargs)


def train_loop(model, tokenizer, train_dataloader, criterion, optimizer):
    model.train()
    # tqdm for a nice progress bar, allow only on rank 0 for cleaner output
    loop = tqdm(train_dataloader, disable=(idr_torch.rank != 0))

    for i, data in enumerate(loop):
        optimizer.zero_grad()

        inputs = tokenizer.batch_encode_plus(
            data['text'],
            return_tensors="pt",
            padding=True,
            truncation=True,
            max_length=512,
        ).to(DEVICE)
        labels = data['label'].to(DEVICE)

        out = model(**inputs)
        loss = criterion(out.logits, labels)

        loss.backward()
        optimizer.step()

        # print next to progress bar
        loop.set_postfix(loss=loss.item())

        if i >= 50:
            loop.close()
            break

    return model


def eval_loop(model, tokenizer, test_dataloader):
    # Torchmetrics metrics works well with distributed training
    metric = BinaryAccuracy().to(DEVICE)
    model.eval()
    # tqdm for a nice progress bar, allow only on rank 0 for cleaner output
    loop = tqdm(test_dataloader, disable=(idr_torch.rank != 0))
    with torch.no_grad():
        for i, data in enumerate(loop):
            inputs = tokenizer.batch_encode_plus(
                data['text'],
                return_tensors="pt",
                padding=True,
                truncation=True,
                max_length=512,
            ).to(DEVICE)
            labels = data['label'].to(DEVICE)

            out = model(**inputs)
            score = metric(out.logits.argmax(dim=1), labels)

            loop.set_postfix(
                # .compute() gather the metric from all ranks and compute the average
                weighted_average_score=metric.compute().item(),
                score=score.item()
            )

            if i >= 50:
                loop.close()
                break

    return metric.compute()


def main(args):
    # Initialize Datasets
    dataset = load_from_disk(args.data_path)

    train_dataloader = torch.utils.data.DataLoader(
        dataset['train'],
        batch_size=args.batch_size,
        num_workers=4,
        pin_memory=True,
        prefetch_factor=2,
        shuffle=True,
    )

    test_dataloader = torch.utils.data.DataLoader(
        dataset['test'],
        batch_size=args.batch_size,
        num_workers=4,
        pin_memory=True,
        prefetch_factor=2,
    )

    # Initialize Model and Tokenizer
    model_path = os.path.join(args.model_dir, args.model_name)
    tokenizer = AutoTokenizer.from_pretrained(model_path)
    # Configure the tokenizer to ensure padding is done right
    if tokenizer.pad_token is None:
        tokenizer.pad_token_id = 0
    tokenizer.padding_side = 'left'

    # Initialize QLoRA Model

    # Configure quantization
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.bfloat16
    )

    # Initialize quantized model
    model = AutoModelForSequenceClassification.from_pretrained(
        model_path,
        quantization_config=bnb_config,
        trust_remote_code=True,
        device_map={"": DEVICE}
    )

    # I'm not sure what this does
    model = prepare_model_for_kbit_training(model)

    # Configure Lora Training
    if "bloom" in args.model_name:  # TODO: not robust at all, to change
        target_modules = ["query_key_value"]

    elif "Llama" in args.model_name:
        target_modules = ["q_proj", "k_proj", "v_proj"]

    else:
        raise ValueError(f"Unknown model name: {args.model_name}")

    config = LoraConfig(
        r=8,
        lora_alpha=32,
        target_modules=target_modules,
        lora_dropout=0.05,
        bias="none",
        task_type="CAUSAL_LM"
    )

    model = get_peft_model(model, config)
    model.config.use_cache = False  # avoid a warning

    # Initialize Optimizer and Criterion
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)

    for epoch in range(args.epochs):
        print_rank_0(f"{'#'*20} Epoch {epoch+1}/{args.epochs} {'#'*20}")
        start_epoch = time()
        model = train_loop(model, tokenizer, train_dataloader, criterion, optimizer)
        accuracy = eval_loop(model, tokenizer, test_dataloader)
        print_rank_0(
            f"\nEpoch: {epoch+1}/{args.epochs} |",
            f"Duration: {(time() - start_epoch):.3f}s | Accuracy: {accuracy}"
        )
        print(
            f"Max memory allocated for GPU {idr_torch.rank}:",
            f"{torch.cuda.max_memory_allocated(DEVICE) / 1024 ** 3:.1f} GB"
        )


if __name__ == "__main__":
    args = parse_args()
    main(args)
