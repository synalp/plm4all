Here are 2 scripts for pretraining LLMs on Jean Zay:

- one with deepspeed
- one with megatron-deepspeed

Note that Megatron-deepspeed is commonly used to pretrain LLM on Jean Zay:
this has been done first for the Bloom models, but also later on for the
[Croissant LLM](https://github.com/CoderPat/croissant-llm-training) model
and more recently for the [Lucie](https://github.com/CoderPat/croissant-llm-training) LLM.

