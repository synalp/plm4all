Experiments with pretraining a Bloom-7b LLM on Jean Zay with DeepSpeed.
The script has been tested for up to a few hours of pretraining; so hyper-parameters should
probably be tuned to avoid instabilities that might occur later during training.
See [eval.md](eval.md) for a first study on the decrease of the loss and efficiency of the script, as a function of
the number of nodes and various hyper-parameters.

The Lucie (mostly French) corpus has been used for this experiment. It shall be published at some point in time
in the Huggingface hub, but you may use of course another corpus of your choice.
Note that the management of corpus loading/shuffling/... is very basic here and should be improved.

**Disclaimer: it's a work-in-progress script that is probably production-ready !**

@author Christophe Cerisara

