# Welcome to PLM4All

PLM4All is a small project dedicated to providing helpfull scripts and guidance
for the NLP community with access to the Jean Zay cluster, in order to facilitate
using, training and finetuning LLMs on Jean Zay.

It is lead by the [Synalp team](https://synalp.loria.fr) from the
[LORIA laboratory](https://www.loria.fr) in Nancy, France.
The core of the work has been achieved by the AI enginneers from the IDRIS (CNRS) team.

The project is now finished: it ran from February 2023 to August 2023.
The follow-up ANR LLM4All project can be found [here](https://synalp.gitlabpages.inria.fr/llm4all).

The detailed results of PLM4All can be found [on gitlab](https://gitlab.inria.fr/synalp/PLM4All).

A summary of the results can be found in this [poster](poster.pdf).

